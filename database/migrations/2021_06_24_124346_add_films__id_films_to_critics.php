<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFilmsIdFilmsToCritics extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('critics', function (Blueprint $table) {
            $table->unsignedBigInteger('films_idFilms');
            $table->foreign('films_idFilms')->references('idFilms')->on('films');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('critics', function (Blueprint $table) {
            $table->dropForeign('films_idFilms');
            $table->dropColumn('films_idFilms');
        });
    }
}
