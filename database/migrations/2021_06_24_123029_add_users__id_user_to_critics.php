<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddUsersIdUserToCritics extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('critics', function (Blueprint $table) {
            $table->unsignedBigInteger('users_idUser');
            $table->foreign('users_idUser')->references('idUser')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('critics', function (Blueprint $table) {
            $table->dropForeign('users_idUser');
            $table->dropColumn('users_idUser');
        });
    }
}
