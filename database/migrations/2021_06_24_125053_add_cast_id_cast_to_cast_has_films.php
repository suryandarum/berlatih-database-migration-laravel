<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCastIdCastToCastHasFilms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cast_has_films', function (Blueprint $table) {
            $table->unsignedBigInteger('cast_idCast');
            $table->foreign('cast_idCast')->references('idCasT')->on('cast');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cast_has_films', function (Blueprint $table) {
            $table->dropForeign('cast_idCast');
            $table->dropColumn('cast_idCast');
        });
    }
}
